# opcua_server

To use the initial version of the opcua_server:


#### Prerequisite
1. Install the latest version of python on your system. 
2. Check if the pip3 is installed: `$ pip3 --version`

#### Once Prerequisites are installed please follow the underlying steps

Step 1: Install the pipenv for setting up the python virtual environment 

`$ pip3 install pipenv`

Step 2: Install the requirements using pipenv

`$ pipenv install`

Step 3:  Start the opcua_server in one terminal

`$ pipenv run python3 main.py`

Step 4: 

For using the OPC-UA Client GUI on a macOS, the dependencies are already present. You can start the opc-ua client as below

1. Start the opcua-client GUI in another terminal

`$ sudo pipenv run opcua-client` (root privileges required for some functionality of opcua-client GUI)

For using the OPC-UA Client GUI on a windows laptop, use UA-Expert. 

Finally:
Once the respective GUIs are installed please use the localhost IP for connecting to the server.

`opc.tcp://127.0.0.1:4840`
