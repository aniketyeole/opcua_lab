# UaExpert - for Windows

Unzip the uaexpert folder and install the UaExpert

![UaExpert](UA_expert/images/UAexpert-View.PNG)


#### Required clients configurations and UaExpert plugins

1. Connect the client with required certificates and primary key

`Server-> Add->  Custom Discovery <add server> -> Authentation Settings <select required certificate and primary key>`

![client configurations](UA_expert/images/OPCUA-Server.PNG)


2. Add Event View plugin

`Document-> Add->  Document Type <Event View> -> Add`

![Event View Plugin](UA_expert/images/OPCUA-Event.PNG)


3. Add Histroy Trend View plugin

`Document-> Add->  Document Type <Histroy Trend View> -> Add`

![Histroy Trend View Plugin](UA_expert/images/OPCUA-History.PNG)