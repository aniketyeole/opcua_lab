import logging
import random
import time
from opcua import ua, uamethod
from src import opcua_server


class UaServer(opcua_server.OPCUA):

    arg = ua.Argument()
    arg.Name = "Change Valve Shaft Location"
    arg.DataType = ua.NodeId(ua.ObjectIds.Int64)
    arg.ValueRank = -1
    arg.ArrayDimensions = []
    arg.Description = ua.LocalizedText("Location")
    outarg = ua.Argument()
    outarg.Name = "Result"
    outarg.DataType = ua.NodeId(ua.ObjectIds.Int64)
    outarg.ValueRank = -1
    outarg.ArrayDimensions = []
    outarg.Description = ua.LocalizedText("New shaft location")

    def __init__(self, ip_addr) -> None:
        super().__init__(ip_addr)
        self.new_shaftlocation = 90

    @uamethod
    def trigger_position(self, parent, location):
        logging.warning(f"changing the location of valve shaft: {location}")
        if location > 180:
            logging.error("Shaft can only rotate from 0-180")
            return self.new_shaftlocation
        self.new_shaftlocation = location
        return location

    def configure_event(self, event, current_speed):
        event.event.Message = ua.LocalizedText(
            f"Maintain Speed at or below {20}, the current recorded speed is {current_speed}")
        event.event.Severity = 1
        event.event.Property = "Action Required: Reduce liquid flow speed" + \
            str(25)
        event.trigger()

    def initiate_server(self):
        self.server_endpoint()
        self.import_uaobject()
        self.set_security_policy()
        self.history_storage()
        servo_object = self.create_uaobject("Control_Valve")
        position = self._add_variable(servo_object, "Valve_Position", "0")
        speed = self._add_variable(servo_object, "Liquid_Flow_Speed", "0")

        self._add_method(
            servo_object, "Change valve position", self.trigger_position, self.arg, self.outarg)

        create_event = self._add_event(servo_object, "Monitor Flow_Speed of Liquid in Valve", ua.ObjectIds.BaseEventType, [
                                       ('Property: ', ua.VariantType.String)])

        self.start_uaserver()
        self.server.historize_node_data_change(position)
        self.server.historize_node_data_change(speed)

        while True:
            position.set_value(f"{self.new_shaftlocation}")
            set_speed = random.randrange(10, 30, 5)
            speed.set_value(f"{set_speed}")
            if set_speed > 20:
                self.configure_event(create_event, set_speed)
            time.sleep(random.randint(4, 10))
