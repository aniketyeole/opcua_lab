#!/usr/local/bin/python
import logging
from signal import signal, SIGINT
from example_app import simulate_ua_server


logging.basicConfig(format='OPCUA-logger: %(asctime)s - %(process)d - [%(levelname)s]-  %(message)s',
                    datefmt='%Y-%m-%dT%H:%M:%S  ',
                    level=logging.INFO)


if __name__ == '__main__':

    initate_servo_server = simulate_ua_server.UaServer("127.0.0.1")
    signal(SIGINT, initate_servo_server.handler)
    initate_servo_server.initiate_server()
