import logging
import sys
from opcua import ua, Server
from opcua.server.history_sql import HistorySQLite

sys.path.insert(0, "..")


class OPCUA():

    def __init__(self, ip_addr) -> None:
        self.server = Server()
        self.ip_addr = ip_addr
        self.config_file = "config/CNC_Machine.xml"
        self.root_certificate = "certificates/certificate-opcua.der"
        self.private_key = "certificates/private-key-opcua.pem"
        self.history_storage_path = "history_data_store.db"
        pass

    def server_endpoint(self):
        return self.server.set_endpoint(f"opc.tcp://{self.ip_addr}:4840/freeopcua/server/")

    def import_uaobject(self):
        self.server.import_xml(self.config_file)

    def set_security_policy(self):
        self.server.set_security_policy([ua.SecurityPolicyType.Basic256Sha256_SignAndEncrypt])
        self.server.load_certificate(self.root_certificate)
        self.server.load_private_key(self.private_key)

    def set_namespace_index(self):
        idx = self.server.get_namespace_index("https://opcua_server.org")
        return idx

    def create_uaobject(self, label):
        object_node = self.server.get_objects_node()
        new_object = object_node.add_object(self.set_namespace_index(), label)
        return new_object

    def _add_variable(self, _object, label, data):
        test_varaible = _object.add_variable(
            self.set_namespace_index(), label, data)
        test_varaible.set_writable()
        return test_varaible

    def history_storage(self):
        self.server.iserver.history_manager.set_storage(HistorySQLite(self.history_storage_path))
        
    def _add_method(self, _object, label, trigger, arg, outarg):
        return _object.add_method(self.set_namespace_index(), label, trigger, [arg], [outarg])

    def _add_event(self, _object, label, basetype, event_property):
        _event_type = self.server.create_custom_event_type(
            self.set_namespace_index(), label, basetype, event_property)
        return self.server.get_event_generator(_event_type, _object)

    #start and stop server

    def start_uaserver(self):
        logging.info("Starting UaServer")
        return self.server.start()

    def stop_uaserver(self):
        # try to catch cntrl+c to stop the server
        logging.info("Stopping UaServer")
        return self.server.stop()

    def handler(self, signal_received, frame):
        logging.warning('SIGINT or CTRL-C detected. Exiting server gracefully')
        self.stop_uaserver()
        sys.exit()
